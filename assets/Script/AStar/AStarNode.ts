// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import AStarCheckTag from "./AStarCheckTag";

const { ccclass, property } = cc._decorator;

@ccclass
export class AStarNode<DATA, TAG> {

  corde: cc.Vec2;
  myIndex: number;
  parentIndex: number;
  customData: DATA;

  myTag: AStarCheckTag<TAG>;

  f: number;//f=g+h;
  g: number;//cost to start point;
  h: number;//cost to end pont;

  visitCount: number;

  constructor() {
    this.parentIndex = -1;
    this.myIndex = -1;
    this.visitCount = 0;
  }

  public hasParent(): boolean {
    return this.parentIndex != -1;
  }
}
