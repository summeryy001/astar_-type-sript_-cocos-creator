// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


const { ccclass, property } = cc._decorator;

@ccclass
export default class PointNode extends cc.Component {

  @property(cc.Label)
  fLabel: cc.Label = null;
  @property(cc.Node)
  light: cc.Node = null;
  @property(cc.Label)
  gLabel: cc.Label = null;
  @property(cc.Label)
  hLabel: cc.Label = null;
  @property(cc.Label)
  dirLabel: cc.Label = null;


  setF(v: number) {
    this.fLabel.string = `${v}`
  }
  setG(v: number) {
    this.gLabel.string = `${v}`
  }
  setH(v: number) {
    this.hLabel.string = `${v}`
  }

  setDir(v: number) {
    this.dirLabel.string = `${v}`;
  }

  lightOn() {
    this.light.active = true;
  }

  lightOff() {
    this.light.active = false;
  }
}
